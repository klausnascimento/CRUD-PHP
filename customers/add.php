<?php 
  require_once('functions.php'); 
  add();
?>

<?php include(HEADER_TEMPLATE); ?>

<h2>Novo Cliente</h2>

<form action="add.php" method="post">
  <!-- area de campos do form -->
  <hr />
  <div class="row">
    <div class="form-group col-md-7">
      <label for="name">Nome / Razão Social</label>
      <input type="text" class="form-control" placeholder="Nome Completo" name="customer['name']">
    </div>

    <div class="form-group col-md-3">
      <label for="cpf_cnpj">CNPJ / CPF</label>
      <input type="text" class="form-control" placeholder="123.456.789-00" name="customer['cpf_cnpj']">
    </div>

    <div class="form-group col-md-2">
      <label for="birthdate">Data de Nascimento</label>
      <input type="text" class="form-control" placeholder="aaaa/dd/mm" name="customer['birthdate']">
    </div>
  </div>
  
  <div class="row">
    <div class="form-group col-md-5">
      <label for="address">Endereço</label>
      <input type="text" class="form-control" name="customer['address']">
    </div>

    <div class="form-group col-md-3">
      <label for="hood">Bairro/Distrito</label>
      <input type="text" class="form-control" name="customer['hood']">
    </div>
    
    <div class="form-group col-md-2">
      <label for="zip_code">CEP</label>
      <input type="text" class="form-control" name="customer['zip_code']">
    </div>
    
    <div class="form-group col-md-2">
      <label for="created">Data de Cadastro</label>
      <input type="text" class="form-control" name="customer['created']" disabled>
    </div>
  </div>
  
  <div class="row">
    <div class="form-group col-md-3">
      <label for="city">Município</label>
      <input type="text" class="form-control" name="customer['city']">
    </div>
    
    <div class="form-group col-md-2">
      <label for="phone">Telefone</label>
      <input type="number" class="form-control" placeholder="00 0000-0000" name="customer['phone']">
    </div>
    
    <div class="form-group col-md-2">
      <label for="mobile">Celular</label>
      <input type="text" class="form-control" placeholder="00 0000-0000" name="customer['mobile']">
    </div>
    
    <div class="form-group col-md-1">
      <label for="state">UF</label>
      <input type="text" class="form-control" name="customer['state']">
    </div>
    
    <div class="form-group col-md-2">
      <label for="ie">Inscrição Estadual</label>
      <input type="text" class="form-control" name="customer['ie']">
    </div>
    
    <div class="form-group col-md-2">
      <label for="ieuf">UF</label>
      <input type="text" class="form-control">
    </div>
  </div>
  
  <div id="actions" class="row">
    <div class="col-md-12">
      <button type="submit" class="btn btn-primary">Salvar</button>
      <a href="index.php" class="btn btn-default">Cancelar</a>
    </div>
  </div>
</form>

<?php include(FOOTER_TEMPLATE); ?>