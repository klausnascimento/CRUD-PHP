Projeto CRUD com Bootstrap, PHP & MySQL

Esse é um projeto bem simples.

1 - Estrutura e conexão com o Banco de Dados;

2 - Os templates de telas e a página inicial;

3 - A tela de listagem (que permite acessar as funções do CRUD);

4 - As telas de cadastro e de edição (equivalente ao Create e Update);

5 - A tela de visualização (equivalente ao Read);

6 - O popup de exclusão (equivalente ao Delete);